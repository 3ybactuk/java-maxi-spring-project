package com.practice.javamaxispringproject.control.merchservice;

import lombok.Data;

@Data
public class AddItemRequest {
    private String name;
    private String description;
    private Integer quantity;
    private Integer price;
}

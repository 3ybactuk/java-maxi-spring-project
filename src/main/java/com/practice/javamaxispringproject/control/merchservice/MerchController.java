package com.practice.javamaxispringproject.control.merchservice;

import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.usecase.merchservice.MerchUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MerchController {
    private final MerchUseCase merchUseCase;

    @Autowired
    public MerchController(MerchUseCase merchUseCase) {
        this.merchUseCase = merchUseCase;
    }

    @GetMapping("/merch_items/list")
    public ResponseEntity<List<MerchItem>> getAllItems() {
        List<MerchItem> merchItemList = merchUseCase.findAll();

        return ResponseEntity.ok(merchItemList);
    }

    @GetMapping("/merch_items/get/{id}")
    public ResponseEntity<MerchItem> getItem(@PathVariable Long id) {
        MerchItem merchItem = merchUseCase.findById(id);

        return ResponseEntity.ok(merchItem);
    }

    @PostMapping("/merch_items/add")
    public ResponseEntity<MerchItem> addItem(@RequestBody AddItemRequest request) {
        MerchItem merchItem = new MerchItem();
        merchItem.setName(request.getName());
        merchItem.setDescription(request.getDescription());
        merchItem.setQuantity(request.getQuantity());
        merchItem.setPrice(request.getPrice());

        return ResponseEntity.ok(merchUseCase.addItem(merchItem));
    }

    @PostMapping("/merch_items/update")
    public ResponseEntity<MerchItem> updateItem(@RequestBody UpdateItemRequest request) {
        MerchItem merchItem = new MerchItem();
        merchItem.setId(request.getId());
        merchItem.setName(request.getName());
        merchItem.setDescription(request.getDescription());
        merchItem.setQuantity(request.getQuantity());
        merchItem.setPrice(request.getPrice());

        merchUseCase.updateItem(merchItem);
        return ResponseEntity.ok(merchItem);
    }

    @PostMapping("/merch_items/remove")
    public ResponseEntity<MerchItem> removeItem(@RequestBody RemoveItemRequest request) {
        Long id = request.getId();
        merchUseCase.removeById(id);
        return ResponseEntity.ok().build();
    }
}

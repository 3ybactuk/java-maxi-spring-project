package com.practice.javamaxispringproject.control.merchservice;

import lombok.Data;

@Data
public class RemoveItemRequest {
    private Long id;
}

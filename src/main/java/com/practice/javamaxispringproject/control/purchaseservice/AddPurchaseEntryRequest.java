package com.practice.javamaxispringproject.control.purchaseservice;

import lombok.Data;

@Data
public class AddPurchaseEntryRequest {
    private Long userId;
    private Long itemId;
    private int itemAmount;
}

package com.practice.javamaxispringproject.control.purchaseservice;

import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.entities.purchaseservice.PurchaseEntry;
import com.practice.javamaxispringproject.entities.userwalletservice.User;
import com.practice.javamaxispringproject.usecase.merchservice.MerchUseCase;
import com.practice.javamaxispringproject.usecase.purchaseservice.PurchaseUseCase;
import com.practice.javamaxispringproject.usecase.userwalletservice.UserUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class PurchaseController {
    private final PurchaseUseCase purchaseUseCase;
    private final UserUseCase userUseCase;
    private final MerchUseCase merchUseCase;

    @Autowired
    public PurchaseController(PurchaseUseCase purchaseUseCase, UserUseCase userUseCase, MerchUseCase merchUseCase) {
        this.purchaseUseCase = purchaseUseCase;
        this.userUseCase = userUseCase;
        this.merchUseCase = merchUseCase;
    }

    @PostMapping("/purchase")
    public ResponseEntity<PurchaseEntry> addPurchaseEntry(@RequestBody AddPurchaseEntryRequest request) {
        MerchItem item = merchUseCase.findById(request.getItemId());
        User user = userUseCase.findUserById(request.getUserId());

        return ResponseEntity.ok(purchaseUseCase.addEntry(user, item, request.getItemAmount()));
    }
}

package com.practice.javamaxispringproject.control.templateprocessor;

import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.entities.purchaseservice.PurchaseEntry;
import com.practice.javamaxispringproject.entities.userwalletservice.User;
import com.practice.javamaxispringproject.usecase.merchservice.MerchUseCase;
import com.practice.javamaxispringproject.usecase.purchaseservice.PurchaseUseCase;
import com.practice.javamaxispringproject.usecase.userwalletservice.UserUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class TemplateController {
    @Autowired
    private UserUseCase userUseCase;

    @Autowired
    private MerchUseCase merchUseCase;

    @Autowired
    private PurchaseUseCase purchaseUseCase;

    @GetMapping("/")
    public String viewHomePage(Model model) {
        return "index";
    }

    @GetMapping("/users")
    public String viewEmployees(Model model, @RequestParam(name = "userQuery", required = false) String userQuery) {
        List<User> users;

        if (userQuery != null && !userQuery.isEmpty()) {
            users = userUseCase.searchUsers(userQuery);
        } else {
            users = userUseCase.findAllUsers();
        }

        model.addAttribute("user_query", userQuery);
        model.addAttribute("user_list", users);

        return "users";
    }

    @GetMapping("/users/updateBalanceForm/{id}")
    public String updateBalanceForm(@PathVariable Long id, Model model) {
        User user = userUseCase.findUserById(id);
        model.addAttribute("user", user);
        return "updateBalanceForm";
    }

    @PostMapping("/users/updateBalance")
    public String updateBalance(@RequestParam Long id, @RequestParam Integer amount) {
        userUseCase.updateBalanceById(id, amount);
        return "redirect:/users";
    }

    @GetMapping("/items")
    public String viewItems(Model model, @RequestParam(name = "itemQuery", required = false) String itemQuery) {
        List<MerchItem> merchItems;

        if (itemQuery != null && !itemQuery.isEmpty()) {
            merchItems = merchUseCase.searchItems(itemQuery);
        } else {
            merchItems = merchUseCase.findAll();
        }

        model.addAttribute("item_query", itemQuery);
        model.addAttribute("item_list", merchItems);
        return "items";
    }

    @GetMapping("/items/updateItemForm/{id}")
    public String updateItemForm(@PathVariable Long id, Model model) {
        MerchItem item = merchUseCase.findById(id);
        model.addAttribute("merchitem", item);
        return "updateItemForm";
    }

    @PostMapping("/items/updateItem")
    public String updateItem(@ModelAttribute MerchItem item) {
        merchUseCase.updateItem(item);
        return "redirect:/items";
    }

    @GetMapping("/items/addItemForm")
    public String addItemForm(Model model) {
        model.addAttribute("merchitem", new MerchItem());
        return "addItemForm";
    }

    @PostMapping("/items/addItem")
    public String addItem(@ModelAttribute MerchItem item) {
        merchUseCase.addItem(item);
        return "redirect:/items";
    }

    @PostMapping("/items/removeItem")
    public String removeItem(@RequestParam Long id) {
        merchUseCase.removeById(id);
        return "redirect:/items";
    }

    @GetMapping("/purchasePage")
    public String showPurchaseForm(Model model,
                                   @RequestParam(name = "userQuery", required = false) String userQuery,
                                   @RequestParam(name = "itemQuery", required = false) String itemQuery) {
        List<User> users;
        List<MerchItem> items;
        List<PurchaseEntry> purchaseEntries = purchaseUseCase.findAll();

        if (itemQuery != null && !itemQuery.isEmpty()) {
            items = merchUseCase.searchItems(itemQuery);
        } else {
            items = merchUseCase.findAll();
        }

        if (userQuery != null && !userQuery.isEmpty()) {
            users = userUseCase.searchUsers(userQuery);
        } else {
            users = userUseCase.findAllUsers();
        }

        model.addAttribute("user_query", userQuery);
        model.addAttribute("item_query", itemQuery);
        model.addAttribute("user_list", users);
        model.addAttribute("item_list", items);
        model.addAttribute("purchase_list", purchaseEntries);
        model.addAttribute("selectedUser", new User());
        model.addAttribute("selectedItem", new MerchItem());
        model.addAttribute("amountOfItems", 1);
        return "purchasePage";
    }

    @PostMapping("/purchaseItem")
    public String purchase(@RequestParam Long selectedUserId, @RequestParam Long selectedItemId, @RequestParam Integer amountOfItems,
                           @ModelAttribute("userQuery") String userQuery,
                           @ModelAttribute("itemQuery") String itemQuery) {
        User selectedUser = userUseCase.findUserById(selectedUserId);
        MerchItem selectedItem = merchUseCase.findById(selectedItemId);

        userUseCase.buyItem(selectedUser, selectedItem, amountOfItems);

        String redirectUrl = "redirect:/purchasePage";
        if (userQuery != null && !userQuery.isEmpty()) {
            redirectUrl += "?userQuery=" + userQuery;
        }
        if (itemQuery != null && !itemQuery.isEmpty()) {
            redirectUrl += (userQuery != null && !userQuery.isEmpty()) ? "&itemQuery=" + itemQuery : "?itemQuery=" + itemQuery;
        }

        return redirectUrl;
    }
}
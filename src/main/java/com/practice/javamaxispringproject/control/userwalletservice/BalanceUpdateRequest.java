package com.practice.javamaxispringproject.control.userwalletservice;

import lombok.Data;

@Data
public class BalanceUpdateRequest {
    private Long id;
    private Integer amount;
}

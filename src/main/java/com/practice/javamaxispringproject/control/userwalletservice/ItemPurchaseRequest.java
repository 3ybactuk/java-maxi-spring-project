package com.practice.javamaxispringproject.control.userwalletservice;

import lombok.Data;

@Data
public class ItemPurchaseRequest {
    private Long userId;
    private Long itemId;
    private int itemAmount;
}

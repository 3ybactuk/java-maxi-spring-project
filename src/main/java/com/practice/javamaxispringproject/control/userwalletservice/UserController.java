package com.practice.javamaxispringproject.control.userwalletservice;

import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.entities.userwalletservice.User;
import com.practice.javamaxispringproject.usecase.merchservice.MerchUseCase;
import com.practice.javamaxispringproject.usecase.userwalletservice.UserUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private final UserUseCase userUseCase;
    private final MerchUseCase merchUseCase;

    @Autowired
    public UserController(UserUseCase userUseCase, MerchUseCase merchUseCase) {
        this.userUseCase = userUseCase;
        this.merchUseCase = merchUseCase;
    }

    @GetMapping("/users/list")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userUseCase.findAllUsers();
        return ResponseEntity.ok(users);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        return ResponseEntity.ok(userUseCase.findUserById(id));
    }

    @GetMapping("users/balance/{id}")
    public ResponseEntity<Integer> getBalanceById(@PathVariable Long id) {
        return ResponseEntity.ok(userUseCase.getBalanceById(id));
    }

    @PostMapping("users/add_balance")
    public ResponseEntity<User> addBalanceById(@RequestBody BalanceUpdateRequest request) {
        return ResponseEntity.ok(userUseCase.addBalanceById(request.getId(), request.getAmount()));
    }

    @PostMapping("users/update_balance")
    public ResponseEntity<User> updateBalanceById(@RequestBody BalanceUpdateRequest request) {
        return ResponseEntity.ok(userUseCase.updateBalanceById(request.getId(), request.getAmount()));
    }

    @PostMapping("users/sub_balance")
    public ResponseEntity<User> subBalanceById(@RequestBody BalanceUpdateRequest request) {
        return ResponseEntity.ok(userUseCase.subBalanceById(request.getId(), request.getAmount()));
    }

    @PostMapping("users/buy_item")
    public ResponseEntity<Boolean> buyItem(@RequestBody ItemPurchaseRequest request) {
        MerchItem item = merchUseCase.findById(request.getItemId());
        User user = userUseCase.findUserById(request.getUserId());

        return ResponseEntity.ok(userUseCase.buyItem(user, item, request.getItemAmount()));
    }
}

package com.practice.javamaxispringproject.database.merchservice;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Интерфейс репозитория базы данных для MerchItems.
 */
@Repository
public interface MerchRepository extends JpaRepository<DBMerchItem, Long> {

    /**
     *
     * Находит элементы товаров в базе данных по заданному имени или описанию.
     * @param name          Имя товара, которое нужно найти.
     * @param description   Описание товара, которое нужно найти.
     * @return Список объектов DBMerchItem, соответствующих имени или описанию.
     */
    List<DBMerchItem> findByNameIgnoreCaseContainingOrDescriptionIgnoreCaseContaining(String name, String description);
}

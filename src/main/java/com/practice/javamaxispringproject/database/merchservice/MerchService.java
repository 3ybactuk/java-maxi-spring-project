package com.practice.javamaxispringproject.database.merchservice;

import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.utils.ModelMapperUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Слой сервиса для товаров.
 */
@Service
public class MerchService {
    private final MerchRepository repository;

    public MerchService(MerchRepository repository) {
        this.repository = repository;
    }

    /**
     * Получает список всех элементов MerchItems в базе данных.
     *
     * @return Список всех элементов MerchItems в виде объектов MerchItem.
     */
    public List<MerchItem> findAll() {
        return ModelMapperUtils.mapList(repository.findAll(), MerchItem.class);
    }

    /**
     * Получает элемент MerchItem по его ID.
     *
     * @param id ID искомого MerchItem.
     * @return MerchItem, если найден, или null, если не найден.
     */
    public MerchItem findById(Long id) {
        Optional<DBMerchItem> itemOptional = repository.findById(id);

        if (itemOptional.isPresent()) {
            return ModelMapperUtils.map(itemOptional.get(), MerchItem.class);
        }

        return null;
    }

    /**
     * Ищет элементы MerchItem по запросу в их имени или описании.
     *
     * @param query Строка запроса для поиска в именах или описаниях элементов MerchItem.
     * @return Список элементов MerchItem, соответствующих поисковому запросу.
     */
    public List<MerchItem> searchItems(String query) {
        return ModelMapperUtils.mapList(repository.findByNameIgnoreCaseContainingOrDescriptionIgnoreCaseContaining(query, query), MerchItem.class);
    }

    /**
     * Добавляет новый элемент MerchItem в базу данных.
     *
     * @param item Добавляемый элемент MerchItem.
     * @return Добавленный элемент MerchItem.
     */
    public MerchItem addItem(MerchItem item) {
        DBMerchItem itemToAdd = ModelMapperUtils.map(item, DBMerchItem.class);

        return ModelMapperUtils.map(repository.save(itemToAdd), MerchItem.class);
    }

    /**
     * Обновляет существующий элемент MerchItem в базе данных.
     *
     * @param item Элемент MerchItem с обновленной информацией.
     * @return Обновленный элемент MerchItem.
     */
    public MerchItem updateItem(MerchItem item) {
        DBMerchItem itemToUpdate = ModelMapperUtils.map(item, DBMerchItem.class);

        return ModelMapperUtils.map(repository.save(itemToUpdate), MerchItem.class);
    }

    /**
     * Удаляет элемент MerchItem из базы данных по его ID.
     *
     * @param id ID удаляемого элемента MerchItem.
     */
    public void removeById(Long id) {
        repository.deleteById(id);
    }
}

package com.practice.javamaxispringproject.database.purchaseservice;

import com.practice.javamaxispringproject.database.merchservice.DBMerchItem;
import com.practice.javamaxispringproject.database.userwalletservice.DBUser;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Data
@Entity
@Table(name = "purchase_entries")
public class DBPurchaseEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "purchase_timestamp")
    private Date purchaseDateTime;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private DBUser user;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id")
    private DBMerchItem item;

    @Column(name = "items_purchased")
    private Integer itemsPurchased;

    @Column(name = "total_cost")
    private Integer totalCost;
}

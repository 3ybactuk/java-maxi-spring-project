package com.practice.javamaxispringproject.database.purchaseservice;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Интерфейс репозитория для записей о покупках в базе данных.
 */
@Repository
public interface PurchaseRepository extends JpaRepository<DBPurchaseEntry, Long> {

    /**
     * Находит все записи о покупках в базе данных и сортирует их по убыванию времени покупки.
     *
     * @return Список объектов DBPurchaseEntry, убывающих по времени покупки.
     */
    List<DBPurchaseEntry> findAllByOrderByPurchaseDateTimeDesc();
}

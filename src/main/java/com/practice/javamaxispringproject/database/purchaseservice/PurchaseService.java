package com.practice.javamaxispringproject.database.purchaseservice;

import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.entities.purchaseservice.PurchaseEntry;
import com.practice.javamaxispringproject.entities.userwalletservice.User;
import com.practice.javamaxispringproject.utils.ModelMapperUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Слой сервиса для записей о покупках.
 */
@Service
public class PurchaseService {
    private PurchaseRepository repository;

    public PurchaseService(PurchaseRepository repository) {
        this.repository = repository;
    }

    /**
     * Получает список всех записей о покупках.
     *
     * @return Список всех записей о покупках.
     */
    public List<PurchaseEntry> findAll() {
        return ModelMapperUtils.mapList(repository.findAllByOrderByPurchaseDateTimeDesc(), PurchaseEntry.class);
    }

    /**
     * Получает запись о покупке по её ID.
     *
     * @param id ID записи о покупке.
     * @return Запись о покупке, если найдена, или null, если не найдена.
     */
    public PurchaseEntry findById(Long id) {
        Optional<DBPurchaseEntry> entryOptional = repository.findById(id);

        if (entryOptional.isPresent()) {
            return ModelMapperUtils.map(entryOptional.get(), PurchaseEntry.class);
        }

        return null;
    }

    /**
     * Добавляет новую запись о покупке.
     *
     * @param purchaseEntry Запись о покупке для добавления.
     * @return Добавленная запись о покупке.
     */
    public PurchaseEntry addEntry(PurchaseEntry purchaseEntry) {
        DBPurchaseEntry entryToAdd = ModelMapperUtils.map(purchaseEntry, DBPurchaseEntry.class);

        return ModelMapperUtils.map(repository.save(entryToAdd), PurchaseEntry.class);
    }
}

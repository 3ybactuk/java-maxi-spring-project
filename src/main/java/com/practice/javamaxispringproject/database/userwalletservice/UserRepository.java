package com.practice.javamaxispringproject.database.userwalletservice;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Интерфейс репозитория для DBUser в базе данных.
 */
@Repository
public interface UserRepository extends JpaRepository<DBUser, Long> {
    /**
     * Находит пользователей в базе данных, соответствующих заданному имени или адресу электронной почты.
     *
     * @param name  Имя пользователя для поиска.
     * @param email Почта пользователя для поиска.
     * @return Список объектов DBUser, соответствующих имени или адресу электронной почты.
     */
    List<DBUser> findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(String name, String email);

}

package com.practice.javamaxispringproject.database.userwalletservice;

import com.practice.javamaxispringproject.entities.userwalletservice.User;
import com.practice.javamaxispringproject.utils.ModelMapperUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Слой сервиса для пользователей.
 */
@Service
public class UserService {
    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    /**
     * Получает список всех пользователей.
     *
     * @return Список всех пользователей.
     */
    public List<User> findAllUsers() {
        return ModelMapperUtils.mapList(repository.findAll(PageRequest.of(0, 100)).toList(), User.class);
    }

    /**
     * Получает пользователя по его ID.
     *
     * @param id ID пользователя.
     * @return Пользователь, если найден, или null, если не найден.
     */
    public User findUserById(Long id) {
        return ModelMapperUtils.map(repository.findById(id), User.class);
    }

    /**
     * Ищет пользователей по запросу в их имени или адресе электронной почты.
     *
     * @param query Запрос для поиска пользователей.
     * @return Список пользователей, соответствующих запросу.
     */
    public List<User> searchUsers(String query) {
        return ModelMapperUtils.mapList(repository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(query, query), User.class);
    }

    /**
     * Получает баланс пользователя по его ID.
     *
     * @param id ID пользователя.
     * @return Баланс пользователя, если пользователь найден, или null, если не найден.
     */
    public Integer getBalanceById(Long id) {
        Optional<DBUser> userOptional = repository.findById(id);

        if (userOptional.isPresent()) {
            return userOptional.get().getBalance();
        }

        return null;
    }

    /**
     * Увеличивает баланс пользователя по его ID на заданное количество.
     *
     * @param id     ID пользователя.
     * @param amount Количество, на которое увеличивается баланс.
     * @return Пользователь с обновленным балансом, если пользователь найден, или null, если не найден.
     */
    public User addBalanceById(Long id, int amount) {
        Optional<DBUser> userOptional = repository.findById(id);

        if (userOptional.isPresent()) {
            User user = ModelMapperUtils.map(userOptional.get(), User.class);
            user.setBalance(user.getBalance() + amount);
            repository.save(ModelMapperUtils.map(user, DBUser.class));

            return user;
        }

        return null;
    }

    /**
     * Обновляет баланс пользователя по его ID до заданного значения.
     *
     * @param id     ID пользователя.
     * @param amount Новое значение баланса.
     * @return Пользователь с обновленным балансом, если пользователь найден, или null, если не найден.
     */
    public User updateBalanceById(Long id, int amount) {
        Optional<DBUser> userOptional = repository.findById(id);

        if (userOptional.isPresent()) {
            User user = ModelMapperUtils.map(userOptional.get(), User.class);
            user.setBalance(amount);
            repository.save(ModelMapperUtils.map(user, DBUser.class));

            return user;
        }

        return null;
    }

    /**
     * Уменьшает баланс пользователя по его ID на заданное количество. Не может уменьшить меньше нуля
     *
     * @param id     ID пользователя.
     * @param amount Количество, на которое уменьшается баланс.
     * @return Пользователь с обновленным балансом, если пользователь найден, или null, если не найден или недостаточный баланс.
     */
    public User subBalanceById(Long id, int amount) {
        Optional<DBUser> userOptional = repository.findById(id);

        if (userOptional.isPresent()) {
            User user = ModelMapperUtils.map(userOptional.get(), User.class);

            if (user.getBalance() - amount < 0) {
                return null;
            }

            user.setBalance(user.getBalance() - amount);
            repository.save(ModelMapperUtils.map(user, DBUser.class));

            return user;
        }

        return null;
    }
}

package com.practice.javamaxispringproject.entities.merchservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class MerchItem {
    private Long id;
    private String name;
    private String description;
    private Integer quantity;
    private Integer price;
}

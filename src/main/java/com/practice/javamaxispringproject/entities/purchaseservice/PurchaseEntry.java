package com.practice.javamaxispringproject.entities.purchaseservice;

import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.entities.userwalletservice.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data @NoArgsConstructor @AllArgsConstructor
public class PurchaseEntry {
    private Long id;
    private Date purchaseDateTime;

    private User user;
    private MerchItem item;

    private Integer itemsPurchased;
    private Integer totalCost;
}

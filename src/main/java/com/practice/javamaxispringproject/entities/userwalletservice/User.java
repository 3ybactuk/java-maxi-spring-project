package com.practice.javamaxispringproject.entities.userwalletservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class User {
    private Long id;
    private String email;
    private String name;
    private int balance;
}

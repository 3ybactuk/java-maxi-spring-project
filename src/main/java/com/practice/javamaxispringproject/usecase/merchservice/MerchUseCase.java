package com.practice.javamaxispringproject.usecase.merchservice;

import com.practice.javamaxispringproject.database.merchservice.MerchService;
import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Слой use case для merch service
 */
@Service
public class MerchUseCase {
    private final MerchService service;

    public MerchUseCase(MerchService service) {
        this.service = service;
    }

    /**
     * Получает список всех MerchItems.
     *
     * @return Список всех MerchItems.
     */
    public List<MerchItem> findAll() {
        return service.findAll();
    }

    /**
     * Получает элемент MerchItem по его ID.
     *
     * @param id ID MerchItem.
     * @return MerchItem, если найден, или null, если не найден.
     */
    public MerchItem findById(Long id) {
        return service.findById(id);
    }

    /**
     * Ищет элементы MerchItem по запросу в их имени или описании.
     *
     * @param query Строка запроса для поиска в именах или описаниях MerchItem.
     * @return Список элементов MerchItem, соответствующих запросу.
     */
    public List<MerchItem> searchItems(String query) {
        return service.searchItems(query);
    }

    /**
     * Добавляет новый MerchItem.
     *
     * @param item Добавляемый MerchItem.
     * @return Добавленный MerchItem.
     */
    public MerchItem addItem(MerchItem item) {
        return service.addItem(item);
    }

    /**
     * Обновляет существующий MerchItem.
     *
     * @param item MerchItem с обновленной информацией.
     * @return Обновленный MerchItem.
     */
    public MerchItem updateItem(MerchItem item) {
        return service.updateItem(item);
    }

    /**
     * Удаляет MerchItem по его ID.
     *
     * @param id ID удаляемого MerchItem.
     */
    public void removeById(Long id) {
        service.removeById(id);
    }
}

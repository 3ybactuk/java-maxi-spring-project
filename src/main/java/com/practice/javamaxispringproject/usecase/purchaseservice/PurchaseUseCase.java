package com.practice.javamaxispringproject.usecase.purchaseservice;

import com.practice.javamaxispringproject.database.purchaseservice.PurchaseService;
import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.entities.purchaseservice.PurchaseEntry;
import com.practice.javamaxispringproject.entities.userwalletservice.User;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Слой use case для записей о покупках.
 */
@Service
public class PurchaseUseCase {
    private final PurchaseService service;

    public PurchaseUseCase(PurchaseService service) {
        this.service = service;
    }

    /**
     * Получает список всех записей о покупках.
     *
     * @return Список объектов PurchaseEntry о покупках.
     */
    public List<PurchaseEntry> findAll() {
        return service.findAll();
    }

    /**
     * Добавляет новую запись о покупке.
     *
     * @param user       Пользователь, совершивший покупку.
     * @param item       Товар, который был куплен.
     * @param itemAmount Количество купленных товаров.
     * @return Добавленная PurchaseEntry.
     */
    public PurchaseEntry addEntry(User user, MerchItem item, int itemAmount) {
        PurchaseEntry newEntry = new PurchaseEntry();

        newEntry.setItem(item);
        newEntry.setUser(user);

        newEntry.setPurchaseDateTime(new Date());
        newEntry.setItemsPurchased(itemAmount);
        newEntry.setTotalCost(itemAmount * item.getPrice());

        return service.addEntry(newEntry);
    }
}

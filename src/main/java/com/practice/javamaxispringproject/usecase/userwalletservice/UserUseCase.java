package com.practice.javamaxispringproject.usecase.userwalletservice;

import com.practice.javamaxispringproject.database.userwalletservice.UserService;
import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.entities.userwalletservice.User;
import com.practice.javamaxispringproject.usecase.purchaseservice.PurchaseUseCase;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Слой use case для операций с User.
 */
@Service
public class UserUseCase {
    private final UserService service;
    private final PurchaseUseCase purchaseUseCase;

    public UserUseCase(UserService service, PurchaseUseCase purchaseUseCase) {
        this.service = service;
        this.purchaseUseCase = purchaseUseCase;
    }

    /**
     * Получает список всех пользователей.
     *
     * @return Список всех пользователей.
     */
    public List<User> findAllUsers() {
        return service.findAllUsers();
    }

    /**
     * Ищет пользователей по запросу в их имени или адресе электронной почты.
     *
     * @param query Запрос для поиска пользователей.
     * @return Список пользователей, соответствующих запросу.
     */
    public List<User> searchUsers(String query) {
        return service.searchUsers(query);
    }

    /**
     * Получает пользователя по его ID.
     *
     * @param id ID пользователя.
     * @return Пользователь, если найден, или null, если не найден.
     */
    public User findUserById(Long id) {
        return service.findUserById(id);
    }

    /**
     * Получает баланс пользователя по его ID.
     *
     * @param id ID пользователя.
     * @return Баланс пользователя, если пользователь найден, или null, если не найден.
     */
    public Integer getBalanceById(Long id) {
        return service.getBalanceById(id);
    }

    /**
     * Увеличивает баланс пользователя по его ID на заданное количество.
     *
     * @param id     ID пользователя.
     * @param amount Количество, на которое увеличивается баланс.
     * @return Пользователь с обновленным балансом, если пользователь найден, или null, если не найден.
     */
    public User addBalanceById(Long id, Integer amount) {
        return service.addBalanceById(id, amount);
    }

    /**
     * Обновляет баланс пользователя по его ID до заданного значения.
     *
     * @param id     ID пользователя.
     * @param amount Новое значение баланса.
     * @return Пользователь с обновленным балансом, если пользователь найден, или null, если не найден.
     */
    public User updateBalanceById(Long id, Integer amount) {
        return service.updateBalanceById(id, amount);
    }

    /**
     * Уменьшает баланс пользователя по его ID на заданное количество. Не может уменьшить меньше нуля
     *
     * @param id     ID пользователя.
     * @param amount Количество, на которое уменьшается баланс.
     * @return Пользователь с обновленным балансом, если пользователь найден, или null, если не найден или недостаточный баланс.
     */
    public User subBalanceById(Long id, Integer amount) {
        return service.subBalanceById(id, amount);
    }

    /**
     * Покупает товар для пользователя.
     *
     * @param user       Пользователь, совершающий покупку.
     * @param item       Товар, который покупается.
     * @param itemAmount Количество покупаемых товаров.
     * @return true, если покупка успешно совершена; false, если на балансе пользователя недостаточно средств.
     */
    public Boolean buyItem(User user, MerchItem item, int itemAmount) {
        int totalCost = item.getPrice() * itemAmount;
        if (user.getBalance() >= totalCost && subBalanceById(user.getId(), totalCost) != null) {
            purchaseUseCase.addEntry(user, item, itemAmount);

            return true;
        } else {
            return false;
        }
    }
}

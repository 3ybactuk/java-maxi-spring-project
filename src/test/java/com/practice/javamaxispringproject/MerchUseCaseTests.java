package com.practice.javamaxispringproject;

import com.practice.javamaxispringproject.entities.merchservice.MerchItem;
import com.practice.javamaxispringproject.usecase.merchservice.MerchUseCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = JavaMaxiSpringProjectApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class MerchUseCaseTests {

    @Autowired
    private MerchUseCase merchUseCase;

    @Test
    public void testFindAll() {
        merchUseCase.addItem(new MerchItem(1L, "Item 1", "Description of Item 1.", 1, 10));
        merchUseCase.addItem(new MerchItem(2L, "Item 2", "Description of Item 2.", 3, 15));
        merchUseCase.addItem(new MerchItem(3L, "Item 3", "Description of Item 3.", 0, 15));

        List<MerchItem> actualItems = merchUseCase.findAll();

        assertEquals(actualItems.size(), 3);
    }

    @Test
    public void testFindById() {
        Long idToFind = 1L;

        MerchItem expectedItem = new MerchItem(idToFind, "Item 1", "Description of Item 1.", 1, 10);
        merchUseCase.addItem(expectedItem);

        MerchItem actualItem = merchUseCase.findById(idToFind);

        assertEquals(expectedItem, actualItem);
    }

    @Test
    public void testAddItem() {
        MerchItem newItem = new MerchItem(null, "Item 1", "Description of Item 1.", 1, 10);
        MerchItem savedItem = new MerchItem(1L, "Item 1", "Description of Item 1.", 1, 10);

        MerchItem result = merchUseCase.addItem(newItem);

        assertEquals(result, savedItem);
    }

    @Test
    public void testUpdateItem() {
        MerchItem newItem = new MerchItem(null, "Item 1", "Description of Item 1.", 1, 10);

        MerchItem addedItem = merchUseCase.addItem(newItem);

        addedItem.setName("Updated item 1");
        merchUseCase.updateItem(addedItem);

        MerchItem result = merchUseCase.findById(addedItem.getId());

        assertEquals(addedItem.getId(), result.getId());
        assertNotEquals(newItem.getName(), result.getName());
        assertEquals(newItem.getDescription(), result.getDescription());
        assertEquals(newItem.getQuantity(), result.getQuantity());
        assertEquals(newItem.getPrice(), result.getPrice());
    }

    @Test
    public void testRemoveById() {
        MerchItem item = new MerchItem(null, "Item 1", "Description of Item 1.", 1, 10);
        MerchItem addedItem = merchUseCase.addItem(item);

        int prevSize = merchUseCase.findAll().size();
        assertNotEquals(0, prevSize);

        Long id = addedItem.getId();

        merchUseCase.removeById(id);

        assertEquals(prevSize - 1, merchUseCase.findAll().size());
    }
}

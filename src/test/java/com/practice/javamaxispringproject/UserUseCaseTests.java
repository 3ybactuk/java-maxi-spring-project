package com.practice.javamaxispringproject;

import com.practice.javamaxispringproject.entities.userwalletservice.User;
import com.practice.javamaxispringproject.usecase.userwalletservice.UserUseCase;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = JavaMaxiSpringProjectApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class UserUseCaseTests {
    @Autowired
    UserUseCase userUseCase;

    @Test
    public void testAddBalanceById() {
        User user = userUseCase.findAllUsers().get(0);

        int initialBalance = user.getBalance();
        int amountToAdd = 50;
        int expectedBalance = initialBalance + amountToAdd;

        User resultUser = userUseCase.addBalanceById(user.getId(), amountToAdd);

        assertEquals(expectedBalance, resultUser.getBalance());
    }

    @Test
    public void testUpdateBalanceById() {
        User user = userUseCase.findAllUsers().get(0);

        int newBalance = 200;

        User resultUser = userUseCase.updateBalanceById(user.getId(), newBalance);

        assertEquals(newBalance, resultUser.getBalance());
    }

    @Test
    public void testSubBalanceById() {
        User user = userUseCase.findAllUsers().get(0);

        int initialBalance = 100;
        userUseCase.updateBalanceById(user.getId(), initialBalance);
        int amountToSubtract = 30;
        int expectedBalance = initialBalance - amountToSubtract;

        User resultUser = userUseCase.subBalanceById(user.getId(), amountToSubtract);

        assertEquals(expectedBalance, resultUser.getBalance());
    }
}
